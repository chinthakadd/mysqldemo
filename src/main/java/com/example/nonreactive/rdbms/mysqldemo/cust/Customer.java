package com.example.nonreactive.rdbms.mysqldemo.cust;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.Id;

@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Customer {

	@Id
	private int customerNumber;

	private String name;

	private String city;

	private String state;

	private String postalCode;

	private String country;

	private String lastName;

	private String firstName;

	private String phone;
}

