package com.example.nonreactive.rdbms.mysqldemo.cust;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.Duration;
import java.time.Instant;
import java.util.List;
import java.util.function.Function;
import java.util.function.Supplier;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@Log4j2
@RequestMapping("/blocking")
public class CustomerController {

    @Autowired
    private CustomerRepository customerRepository;

    @GetMapping(path = "/customers", produces = APPLICATION_JSON_VALUE)
    public List<Customer> getBlockingCustomers() {

        log.info("fetching all customers in blocking way. ");
        return timeLapseRecorder.apply(customerRepository::findAll);
    }

    @GetMapping(path = "/delayed/customers", produces = APPLICATION_JSON_VALUE)
    public List<Customer> getBlockingCustomerWithDelay(@RequestParam(name = "delay") Float delay) {

        log.info("fetching all customers in blocking way with delay " + delay);
        return timeLapseRecorder.apply(() -> customerRepository.getCustomersWithDelay(delay));
    }


    private static Function<Supplier<List<Customer>>, List<Customer>> timeLapseRecorder = customerSupplier -> {

        Instant start = Instant.now();
        List<Customer> customers = customerSupplier.get();
        Instant finish = Instant.now();
        log.info("time taken in ms :  " + Duration.between(start, finish).toMillis());
        return customers;
    };
}
