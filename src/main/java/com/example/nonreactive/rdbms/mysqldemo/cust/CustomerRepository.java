package com.example.nonreactive.rdbms.mysqldemo.cust;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, String> {

    @Query(nativeQuery = true, value = "call GET_CUSTOMERS_WITH_DELAY(:delay);")
    List<Customer> getCustomersWithDelay(@Param("delay") Float delay);
}
